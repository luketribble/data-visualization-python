
# coding: utf-8

# In[6]:

import statistics #import library to find statistics about dataset
import random # import random number functionality
import matplotlib.pyplot as plt # import plotting functionality
numberOfLoops = 10000 # we want to make 100 random numbers
randomUniform = [] # create an empty array to hold those numbers
for n in range(numberOfLoops): # loop 100 times...
 randomUniform.append(random.gauss(0, 2)) # ... making a new random number each loop
plt.hist(randomUniform,100) # plot it!
plt.show()

statistics.mean(randomUniform) #find the average of the dataset