This is a basic python script that loops over a series of randomly generated numbers and displays the results onto a histogram.
The code can be manipulated and modified with different library functions to alter the graph outputted.

Created and ran with Jupyter Notebook online.